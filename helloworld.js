#!/usr/local/bin/node

var log  = function (msg){
	log_index++; // evil global
	var prefix="nd"+log_index+": ";
	console.log(prefix+msg);
}

function makelog (prefix){
	return function(msg){
	log_index++; // evil global
	var newprefix=prefix+' '+log_index+": ";
	console.log(newprefix+msg);
	}
}

function makelog2 (prefix){
	var log_index=0;
	return function(msg){
	log_index++; // evil global
	var newprefix=prefix+' '+log_index+": ";
	console.log(newprefix+msg);
	}
}

function removeSpaces(text){
    var str;
    str = text.toString();
    str = str.replace(/\s/g, '');
    log("removeSpaces - got "+text +', returning '+str);
    return str;
};

logJS= makelog('JavaScript');
logBS = makelog2('Basic');
console.log("Hello World from yer Basic console.log()");

var log_index=0; 

log("Ooh look!");
log("JavaScript on the shell!");
log("-- No nuffink required --");

logJS("I like coffee");
logJS("I like tea");

logBS("10 PRINT 'RICHARD'");
logBS("20 GOTO 10");

var str = '20 0 	81   264#';
logJS(removeSpaces(str));
// console.clear(); clears the screen
//console.count("alpha");
//console.count("alpha");
//console.count("beta");
//	console.trace();



