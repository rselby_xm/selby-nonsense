#!/usr/bin/env node
console.log(`Setting an object key via a variable`);

let monitor_id = 'shmonitor_id';

let  payload = {
      "resolve": [{ monitor_id: "qwerty123" }]
};

console.log(JSON.stringify(payload));	//{"resolve":[{"monitor_id":"qwerty123"}]}

let  payload2 = {
      "resolve": [{ [monitor_id]: "qwerty123" }]	//{"resolve":[{"shmonitor_id":"qwerty123"}]}
};

console.log(JSON.stringify(payload2));
