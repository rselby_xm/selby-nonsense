#!/usr/bin/env node
log = msg => console.log(msg);
let output={};
let string = "Isn't it nice to think that tomorrow is a new day with no mistakes in it yet?";
const origLen = string.length;
let limit = 24;
let includeEllipses = false
let cleanWordBreak =  true;

log(`Truncate step started`);

if (origLen<=limit) { // easy case
    output.string = string;
    output.isTruncated = false;
    output['Chopped Char Count']= 0;
    log(`Truncate step finished with no changes`);
    return;    
}

let outStr ='';
let chopCount = 0 ;
let maxLen = limit;

if (includeEllipses && origLen>3)
    maxLen = limit - 3; // leave room for ...

outStr = string.substring(0, maxLen).trim();

    
if (cleanWordBreak) {    
    
    let matches = outStr.match(/(\s+\w*)$/);
    if(matches){
        let trimMe = matches[1];
		log(`trimMe is ${trimMe}`);
        outStr = outStr.replace(new RegExp(trimMe+'$'),'');
		log (`outStr is ${outStr}||`);
    }
}

chopCount = origLen - outStr.length;    

if (includeEllipses)
    outStr += '...';

        
output.string = outStr;
output.isTruncated = true;
output['Chopped Char Count']= chopCount;
log(`isTrucated ${output.isTruncated}; choppedCharCount ${chopCount}; outStr ${outStr.length}`);
log(`Truncate step finished with ||${outStr}||`);
