#!/usr/bin/env node

// Testing out my phone number tidy-up function
const log = msg=>{ console.log(`${msg}`);} // Requested by Mike Wieggers 
log('Phonefix started');

let phoneNumberBefore;
phoneNumberBefore = '0208527 2513';
phoneNumberBefore = '447470219340';
phoneNumberBefore = '044 7904 600895';


let device={};
device.phoneNumber = phoneNumberBefore;

device = deviceTransform(device);

let phoneNumberAfter = device.phoneNumber;

log(`Before ${phoneNumberBefore} => After ${phoneNumberAfter}`);

function deviceTransform  (device, index, devices)  {
	//log('DEVICE before' +JSON.stringify(device));
	
	//"owner":"Suzanne.Yates" => suzanne.yates
	if (device.owner)	{
		let owner = device.owner.toLowerCase();
		//log(`Device.owner Before ${device.owner} after ${owner}`);
		device.owner = owner;
	}

	//"targetName":"Suzanne.Yates|Personal Mobile",	=> suzanne.yates|Personal Mobile
	if (device.targetName) {
		let parts = device.targetName.split('|');
		let tn = parts[0].toLowerCase() + '|'+ parts[1];
		//log(`Device.targetName Before ${device.targetName} after ${tn}`)
		device.targetName = tn;
	}
	
	
    // Assumes all phone numbers are UK numbers
	// Transform to E164 for the UK, i.e. +44 prefix
	if (device.phoneNumber) {
     
	 	let phoneNumber = device.phoneNumber;
		
        // Remove everything except a leading + and numbers
        // ex: '+1#(404).123www-4567;ext=123' becomes '+14041234567'
        phoneNumber = phoneNumber.replace(/[^+\d]/g, "");
		
		// At this point there may be nothing left, e.g. when input is 'n/a'
		if (phoneNumber.length===0){		
			//log(`Device.phoneNumber Before ${device.phoneNumber} after EMPTY`);
			return false;  // Best way to handle missing phone number
		}
		
	 	// Remove any leading 0
		if (phoneNumber.match(/^0+/)){
			phoneNumber = phoneNumber.replace(/^0+/,'');
		}
		
		
        // If it starts 44 (for the UK but no +) we add a +
        if (phoneNumber.startsWith('44')) {
            phoneNumber = '+' + phoneNumber;
        }

        
		// Get rid of any stray zeros after the country code
		if (phoneNumber.startsWith('+440')){
			phoneNumber = '+44' + phoneNumber.substr(4);
		}
        //add a +44 if the phone number doesn't already have it.
        else if ( !phoneNumber.startsWith('+44')) {
            phoneNumber = '+44' + phoneNumber;
        }
			
		//log(`Device.phoneNumber Before ${device.phoneNumber} after ${phoneNumber}`);
		
		
		device.phoneNumber = phoneNumber;

	}

	//log('DEVICE after ' +JSON.stringify(device));	
	return device;
}
