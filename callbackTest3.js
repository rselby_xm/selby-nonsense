#!/usr/local/bin/node
var logMe = true;
const log = makeLog('NJS');
log('++ Fiddling around with Callbacks in Node 3 ++');

// https://nodejs.org/en/knowledge/errors/what-are-the-error-conventions/
// Note: when calling the same asynchronous function twice like this, you are in a race condition.
// You have no way of knowing for certain which callback will be called first when calling the functions in this manner.
try {
	isTrue(true, (error, retval) =>  {
		log('In anon()');
  	  	if (error) {
    		log(error); return;
  	  	}
		log(retval);
	});
	isTrue(true, callback);

	isTrue(false, callback);
		
}
catch(e){
	log('Caught exception. '+e);
	
}

function isTrue(value, callback) {
	if (value === true) {
   	 	callback(null, "Value was true.");
 	}
  	else {
    	throw new Error("Value is not true!");
  	}
}

function callback(error, retval) {
	log('In callback()');
	
 	 if (error) {
    	 log(error);
   	  	return;
  	}
  	log(retval);
}




// Logger function
function makeLog(prefix){
	var log_index=0; 
	// returns anonymous function
	return function(msg)
	{
		log_index++;
		var newPrefix=prefix+' '+log_index+": ";
	
		if (!logMe)
			return;
		
		if (typeof msg == 'string')
			console.log(newPrefix+msg);
		
		else if ( msg.name=='Error'){
			//console.log(newPrefix+ msg.constructor.name +' '+ msg.name);
			console.log(newPrefix+ '->'+msg+'<-');			
		}
		else if (typeof msg =='undefined')
			console.log(newPrefix+'<x>');
		
		else {
			console.log(typeof msg + ' '+msg.constructor.name) ;
			console.log(newPrefix+ JSON.stringify(msg));
		}
	}
}
