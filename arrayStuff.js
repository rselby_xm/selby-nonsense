#!/usr/bin/env node
const log = msg => console.log(msg);
log(`Looking at more array stuff`);


let list=[];
list[0]='zero';
list[1]='one';
list[7]='seven';

log(JSON.stringify(list));

list[3]='three';
list[6]='six';
log(JSON.stringify(list));

addMore(list);
log(JSON.stringify(list));

function addMore(list) {
	list.push('ten')
	list[2]='two';
	return;
}

log(`And now for sorting Slack message lines via ts`);
let msg=[];

msg.push({ts:1621975016.003200 , text: 'Look up to the sky and seeeee'});
msg.push({ts:1621975017.003000 , text: 'I\'m just a poor boy'});
msg.push({ts:1621975018.003000 , text: 'I need no sympathy'});
msg.push({ts:1621975015.003000 , text: 'Caught in a landslide'});
msg.push({ts:1621975016.003000 , text: 'No escape from reality'});
msg.push({ts:1621975016.003100 , text: 'Open your eyes'});
msg.push({ts:1621975013.003000 , text: 'Is this the real life?'});
msg.push({ts:1621975014.003000 , text: 'Is this the just fantasy?'});
log(JSON.stringify(msg));

msg.sort( (a,b) =>  (a.ts-b.ts));
log(JSON.stringify(msg));
