#!/usr/local/bin/node
log = makeLogger('CS');
var logMe = true;
log("Fiddling with conversions for CreditSuisse");

var regions = "AM,CH,EMEA";

var out = str2Json(regions);

//var a1 = [];
if (typeof(a1)=='undefined'){
	log("a1 is undefined");
}
var a1
if (!a1) { 
		log("Test for !a1 worked");
}

a1=[];
if (!a1.length) { 
		log("Test empty array for !a1 worked");
}


function str2Json(str) {
	log = makeLogger('str2json');
	log("Got" + str);
	var out = '';
	var arr = str.split(',');
	var out = JSON.stringify(arr);
	log("Returning "+ out);	
	return out;
}



function makeLogger(prefix){
	var log_index=0; 
	// returns anonymous function
	return function(msg)
	{
		log_index++;// like a static variable
		var newPrefix=prefix+' '+log_index+": ";
	
		if (logMe)
			console.log(newPrefix+msg);
	}
}